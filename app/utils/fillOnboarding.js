var Onboarding = require('../models/Onboarding');


module.exports  = {
fillGenericOnboarding : function () {    
    var welcomeTitle = "Welcome!"; 
    var welcomeDescription = "Find great destinations and fantastic deals for your perfect holiday using the TUI apps";
    var languageTitle = "Language Setup";
    var languageArray = ["Bélgium","France","Morroco"];
    var offersTitle = "Offers & Deals";
    var offersDescription = "Let us know where you're based so we can make sure we show you the best offers and deals for your country. If you change mind later you can update this in Settings";
    var overviewTitle = "The perfect travel app";
    var overviewDescription = "With this app you can explore new destinations and find our latest deals. If you're already booked you can add your trip to see your itinerary, guides and extras";
    var conclusionTitle = "You're good to go!"
    var conclusionDescription = "That's it all set up! Don't forget if you have any suggestions, feedback or need some help you can contact us via the app"; 

    var onboardingTest = new Onboarding (welcomeTitle,welcomeDescription,languageTitle,languageArray,offersTitle,offersDescription,overviewTitle,overviewDescription,conclusionTitle,conclusionDescription);

    return onboardingTest;

},

fillNordicOnboarding : function () {    
    var welcomeTitle = "Welcome!"; 
    var welcomeDescription = "Find great destinations and fantastic deals for your perfect holiday using the TUI apps";
    var languageTitle = null;
    var languageArray = null;
    var offersTitle = null;
    var offersDescription = null;
    var overviewTitle = "The perfect travel app";
    var overviewDescription = "With this app you can explore new destinations and find our latest deals. If you're already booked you can add your trip to see your itinerary, guides and extras";
    var conclusionTitle = "You're good to go!"
    var conclusionDescription = "That's it all set up! Don't forget if you have any suggestions, feedback or need some help you can contact us via the app"; 
    
    var onboardingTest = new Onboarding (welcomeTitle,welcomeDescription,languageTitle,languageArray,offersTitle,offersDescription,overviewTitle,overviewDescription,conclusionTitle,conclusionDescription);
    
    return onboardingTest;
    },
    
    fillUkDeOnboarding : function () {    
    var welcomeTitle = "Welcome!"; 
    var welcomeDescription = "Find great destinations and fantastic deals for your perfect holiday using the TUI apps";
    var languageTitle = null;
    var languageArray = null;
    var offersTitle = "Offers & Deals";
    var offersDescription = "Let us know where you're based so we can make sure we show you the best offers and deals for your country. If you change mind later you can update this in Settings";
    var overviewTitle = "The perfect travel app";
    var overviewDescription = "With this app you can explore new destinations and find our latest deals. If you're already booked you can add your trip to see your itinerary, guides and extras";
    var conclusionTitle = "You're good to go!"
    var conclusionDescription = "That's it all set up! Don't forget if you have any suggestions, feedback or need some help you can contact us via the app"; 
        
    var onboardingTest = new Onboarding (welcomeTitle,welcomeDescription,languageTitle,languageArray,offersTitle,offersDescription,overviewTitle,overviewDescription,conclusionTitle,conclusionDescription);
        
    return onboardingTest;
    }
}

