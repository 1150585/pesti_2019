
function Onboarding (welcomeTitle, welcomeDescription,languageTitle,languageArray,offersTitle,offersDescription,overviewTitle,overviewDescription,conclusionTitle,conclusionDescription) {
    this.welcomeTitle = welcomeTitle || null;
    this.welcomeDescription = welcomeDescription || null;
    this.languageTitle = languageTitle || null;
    this.languageArray = languageArray || null;
    this.offersTitle = offersTitle || null;
    this.offersDescription = offersDescription || null;
    this.overviewTitle = overviewTitle || null;
    this.overviewDescription = overviewDescription || null;
    this.conclusionTitle = conclusionTitle || null;
    this.conclusionDescription = conclusionDescription || null; 
}
module.exports = Onboarding;