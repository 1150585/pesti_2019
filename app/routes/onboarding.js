var express = require('express');
var router = express.Router();
var utils = require ('../utils/fillOnboarding')

// URL Base + Source Market
router.get('/NL',function (req,res){
    var body = utils.fillNordicOnboarding();
    res.send(body);
});

router.get('/NO',function (req,res){
    var body = utils.fillNordicOnboarding();
    res.send(body);
});

router.get('/UK',function (req,res){
    var body = utils.fillUkDeOnboarding();
    res.send(body);
});

router.get('/DE',function (req,res){
    var body = utils.fillUkDeOnboarding();
    res.send(body);
});

router.get('/BE',function (req,res){
    var body = utils.fillGenericOnboarding();
    res.send(body);
});

router.get('/fly',function (req,res){
    var body = utils.fillGenericOnboarding();
    res.send(body);
});



// Headers - App-locale define o source market
router.get('/', function (req, res) {
    switch(req.header('App-locale')) { 
    case 'NL':
        var body = utils.fillNordicOnboarding();
        res.send(body);
        break;
    
    case 'NO':
        var body = utils.fillNordicOnboarding();
        res.send(body);
        break;
    
    case 'UK' :
        var body = utils.fillUkDeOnboarding();
        res.send(body);
        break;

    case 'DE':
        var body = utils.fillUkDeOnboarding();
        res.send(body);
        break;

    case 'BE':
        var body = utils.fillGenericOnboarding();
        res.send(body);
        break;

    case 'fly':
        var body = utils.fillGenericOnboarding();
        res.send(body);
        break;
    default:
    var responseError = { "status": 406, "Message": "Not Acceptable" };
    res.send(responseError);
    }       
  });
  
module.exports = router;