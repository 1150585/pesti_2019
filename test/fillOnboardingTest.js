var utils = require ('../app/utils/fillOnboarding');
var Onboarding = require('../app/models/Onboarding');

var assert = require('assert');


describe('Fill Onboarding Methods', function() {
  describe('#fillGenericOnboarding', function() {
    it('should return the same object', function() {
        var welcomeTitle1 = "Welcome!"; 
        var welcomeDescription1 = "Find great destinations and fantastic deals for your perfect holiday using the TUI apps";
        var languageTitle1 = "Language Setup";
        var languageArray1 = ["Bélgium","France","Morroco"];
        var offersTitle1 = "Offers & Deals";
        var offersDescription1 = "Let us know where you're based so we can make sure we show you the best offers and deals for your country. If you change mind later you can update this in Settings";
        var overviewTitle1 = "The perfect travel app";
        var overviewDescription1 = "With this app you can explore new destinations and find our latest deals. If you're already booked you can add your trip to see your itinerary, guides and extras";
        var conclusionTitle1 = "You're good to go!"
        var conclusionDescription1 = "That's it all set up! Don't forget if you have any suggestions, feedback or need some help you can contact us via the app"; 
        var onboardingTest1 = new Onboarding (welcomeTitle1,welcomeDescription1,languageTitle1,languageArray1,offersTitle1,offersDescription1,overviewTitle1,overviewDescription1,conclusionTitle1,conclusionDescription1);
        assert.deepEqual(onboardingTest1, utils.fillGenericOnboarding())
    });

    it('should return a different object', function() {
        var welcomeTitle1 = null; 
        var welcomeDescription1 = null;
        var languageTitle1 = null;
        var languageArray1 = null;
        var offersTitle1 = null;
        var offersDescription1 = null;
        var overviewTitle1 = null;
        var overviewDescription1 = null;
        var conclusionTitle1 = null;
        var conclusionDescription1 = null; 
        var onboardingTest1 = new Onboarding (welcomeTitle1,welcomeDescription1,languageTitle1,languageArray1,offersTitle1,offersDescription1,overviewTitle1,overviewDescription1,conclusionTitle1,conclusionDescription1);
        assert.notDeepEqual(onboardingTest1, utils.fillGenericOnboarding())
    });
  });
  describe('#fillNordicOnboarding', function() {
    it('should return the same object', function() {
        var welcomeTitle2 = "Welcome!"; 
        var welcomeDescription2 = "Find great destinations and fantastic deals for your perfect holiday using the TUI apps";
        var languageTitle2 = null;
        var languageArray2 = null;
        var offersTitle2 = null;
        var offersDescription2 = null;
        var overviewTitle2 = "The perfect travel app";
        var overviewDescription2 = "With this app you can explore new destinations and find our latest deals. If you're already booked you can add your trip to see your itinerary, guides and extras";
        var conclusionTitle2 = "You're good to go!"
        var conclusionDescription2 = "That's it all set up! Don't forget if you have any suggestions, feedback or need some help you can contact us via the app"; 
        var onboardingTest2 = new Onboarding (welcomeTitle2,welcomeDescription2,languageTitle2,languageArray2,offersTitle2,offersDescription2,overviewTitle2,overviewDescription2,conclusionTitle2,conclusionDescription2);
        assert.deepEqual(onboardingTest2, utils.fillNordicOnboarding())
    });

    it('should return a different object', function() {
        var welcomeTitle2 = null; 
        var welcomeDescription2 = null;
        var languageTitle2 = null;
        var languageArray2 = null;
        var offersTitle2 = null;
        var offersDescription2 = null;
        var overviewTitle2 = null;
        var overviewDescription2 = null;
        var conclusionTitle2 = null;
        var conclusionDescription2 = null; 
        var onboardingTest2 = new Onboarding (welcomeTitle2,welcomeDescription2,languageTitle2,languageArray2,offersTitle2,offersDescription2,overviewTitle2,overviewDescription2,conclusionTitle2,conclusionDescription2);
        assert.notDeepEqual(onboardingTest2, utils.fillNordicOnboarding())
    });
  });

  describe('#fillUkDeOnboarding', function() {
    it('should return the same object', function() {
        var welcomeTitle3 = "Welcome!"; 
        var welcomeDescription3 = "Find great destinations and fantastic deals for your perfect holiday using the TUI apps";
        var languageTitle3 = null;
        var languageArray3 = null;
        var offersTitle3 = "Offers & Deals";
        var offersDescription3 = "Let us know where you're based so we can make sure we show you the best offers and deals for your country. If you change mind later you can update this in Settings";
        var overviewTitle3 = "The perfect travel app";
        var overviewDescription3 = "With this app you can explore new destinations and find our latest deals. If you're already booked you can add your trip to see your itinerary, guides and extras";
        var conclusionTitle3 = "You're good to go!"
        var conclusionDescription3 = "That's it all set up! Don't forget if you have any suggestions, feedback or need some help you can contact us via the app"; 
        var onboardingTest3 = new Onboarding (welcomeTitle3,welcomeDescription3,languageTitle3,languageArray3,offersTitle3,offersDescription3,overviewTitle3,overviewDescription3,conclusionTitle3,conclusionDescription3);
        assert.deepEqual(onboardingTest3, utils.fillUkDeOnboarding())
    });

    it('should return a different object', function() {
        var welcomeTitle3 = null; 
        var welcomeDescription3 = null;
        var languageTitle3 = null;
        var languageArray3 = null;
        var offersTitle3 = null;
        var offersDescription3 = null;
        var overviewTitle3 = null;
        var overviewDescription3 = null;
        var conclusionTitle3 = null;
        var conclusionDescription3 = null; 
        var onboardingTest3 = new Onboarding (welcomeTitle3,welcomeDescription3,languageTitle3,languageArray3,offersTitle3,offersDescription3,overviewTitle3,overviewDescription3,conclusionTitle3,conclusionDescription3);
        assert.notDeepEqual(onboardingTest3, utils.fillUkDeOnboarding())
    });
  });

});