var express    = require('express');       
var app        = express();                
var bodyParser = require('body-parser');

var onboardingRouter = require('./app/routes/onboarding');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//// set port
var port = process.env.PORT || 8080;        

// Route Instance
var router = express.Router();        

// Register ROUTES
app.use('/api', router);
app.use('/onboarding',onboardingRouter)

// Start server
app.listen(port);
console.log('Magic happens on port ' + port);